import { MoladinExpectedConditionCommand } from './moladin.expected.condision.commands';
import { MobileElementCommand } from './moladin.element.commands';
import { MobileSelectCommand } from './moladin.select.commands';
import { MobileScollCommand } from './moladin.scroll.commands';
import { MoladinUtilityCommand } from './moladin.utility.commands';
import { MoladinSwipeCommand } from './moladin.swipe.commands';

const elementCondition = new MoladinExpectedConditionCommand();
const elementFinder = new MobileElementCommand();
const elementSelect = new MobileSelectCommand();
const elementScroll = new MobileScollCommand();
const elementSwipe = new MoladinSwipeCommand();
const utility = new MoladinUtilityCommand();

export {
    elementCondition,
    elementFinder,
    elementSelect,
    elementScroll,
    elementSwipe,
    utility,
}