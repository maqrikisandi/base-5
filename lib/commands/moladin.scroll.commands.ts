import { MobileElementCommand } from "./moladin.element.commands";
import { MoladinExpectedConditionCommand } from "./moladin.expected.condision.commands";
import { MoladinSwipeCommand } from "./moladin.swipe.commands";

export class MobileScollCommand {
    _moladinExpectCondition: MoladinExpectedConditionCommand;
    _moladinSwipe: MoladinSwipeCommand;
    _elementCommand: MobileElementCommand;

    constructor(){
        this._moladinExpectCondition = new MoladinExpectedConditionCommand ();
        this._moladinSwipe = new MoladinSwipeCommand();
        this._elementCommand = new MobileElementCommand();
    }

    byUISelectorScrollToElement = async (scrollableClassName:string, stringDirection:string, targetElementSelector:string): Promise<void> => {
        const scrollableLocator = `new UiSelector().className(${scrollableClassName}).scrollable(true)`;
        const elementID = await this._elementCommand.finder(scrollableLocator, 'UISelector');
        const options = {
            elementId: elementID,
            selector: targetElementSelector,
            strategy: '-android uiautomator',
            direction: stringDirection,
            maxSwipes: 30,
        };
        await driver.execute('mobile: scroll', options);
    }

    byScreenSwipeScrollToElement = async (locator:string, swipeDirection:string, strategy:string = 'CSS || XPATH'): Promise<void> => {
        let counterSwipe = 0;
        let elementDisplayed = await this._moladinExpectCondition
            .isElementDisplayed(locator, strategy);

        while (!elementDisplayed && counterSwipe < 50) {
            await this._moladinSwipe.screenSwipe(swipeDirection);
            elementDisplayed = await this._moladinExpectCondition
                .isElementDisplayed(locator, strategy);
            counterSwipe += 1;
        }
    }

    byDragActionScrollToElement = async (locator:string, swipeDirection:string, strategy:string = 'xpath'): Promise<void> => {
        let counterSwipe = 0;
        let elementDisplayed = await this._moladinExpectCondition
            .isElementDisplayed(locator, strategy);

        while (!elementDisplayed && counterSwipe < 10) {
            await this._moladinSwipe
                .byLocatorStrategySwipedElement(strategy, locator, swipeDirection);
            elementDisplayed = await this._moladinExpectCondition
                .isElementDisplayed(locator, strategy);
            counterSwipe += 1;
        }
    }
}